Introduction
============

Merchant e-Solutions APIs implemented in a Python SDK.

###Requirements
The only external dependency is httplib2.  It can be found here:
https://github.com/jcgregorio/httplib2

###Basic Usage
```python
	from gateway.Gateway import GatewayRequest, TransactionType, GatewayUrl

	g = GatewayRequest(GatewayUrl.Cert, TransactionType.Sale)
	g.addCredentials('profile_id_here', 'profile_key_here')
	g.addCardData('4012888812348882', '1216')
	g.addAmount('1.00')
	saleResp = g.run()

	print('Sale error code: '+saleResp.getErrorCode())
	print('Sale resp text: '+saleResp.getRespText())
```
    