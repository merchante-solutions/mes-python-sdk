'''
Created on Dec 24, 2012

@author: brice
'''
import httplib2

class LibHttp:
    def __init__(self, url, request):
        self.requestUrl = url
        self.requestString = request

    def run(self):
        h = httplib2.Http()
        headers = {'Content-type': 'application/x-www-form-urlencoded'}
        resp, content = h.request(self.requestUrl, 'POST', self.requestString, headers)
        self.rawResponse = content;
        self.headers = resp;
